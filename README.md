# Loan Calculator

- https://andre.miras.gitlab.io/loan-calculator/ (broken SSL)
- http://andre.miras.gitlab.io/loan-calculator/ (no SSL)


## Run
```sh
yarn start
```

## Test
```sh
yarn lint
yarn test
```
